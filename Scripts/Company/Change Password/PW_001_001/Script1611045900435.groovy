import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Company/Update Profile/icon_profile'))

WebUI.click(findTestObject('Company/Update Profile/submenu_profile'))

WebUI.click(findTestObject('Company/Change Password/icon_editPassword'))

WebUI.setText(findTestObject('Company/Change Password/txt_oldPass'), oldPass)

WebUI.setText(findTestObject('Company/Change Password/txt_newPass'), newPass)

WebUI.setText(findTestObject('Company/Change Password/txt_confirmPass'), confirmPass)

WebUI.click(findTestObject('Company/Change Password/button_Save'))

WebUI.mouseOver(findTestObject('err_message_css'))

WebUI.verifyMatch(WebUI.getText(findTestObject('err_message_css')).trim(), expect_rs, false)

WebUI.refresh()

WebUI.acceptAlert()

WebUI.delay(10)

WebUI.click(findTestObject('Company/Update Profile/icon_profile'))

WebUI.click(findTestObject('Company/Logout'))

WebUI.acceptAlert()

WebUI.setText(findTestObject('Company/Login/txt_username'), username)

WebUI.setText(findTestObject('Company/Login/txt_password'), newPass)

WebUI.setText(findTestObject('Company/Login/txt_captcha'), captcha)

WebUI.click(findTestObject('Company/Login/btn_login'))

