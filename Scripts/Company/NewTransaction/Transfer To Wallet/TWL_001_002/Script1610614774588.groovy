import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint

import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/menu_NewTransaction'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_BatchPayment'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Next_1'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Transfer to Wallet'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Next_1'))

CustomKeywords.'help.UploadFile.uploadFile'(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Upload'), uploadFile)

WebUI.delay(1)

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Next_1'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Next_1'))

WebUI.check(findTestObject('Company/New Transaction/Transfer To Wallet/cbx_AssigneeChecker'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/txt_duedate'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_duedate_OK'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/button_Save'))

WebUI.verifyElementText(findTestObject('Company/New Transaction/Transfer To Wallet/verify_newRequest'), verifyNewRequest)

