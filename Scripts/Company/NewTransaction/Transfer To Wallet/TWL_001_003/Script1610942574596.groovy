import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/menu_NewTransaction'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_BatchPayment'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Next_1'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Transfer to Wallet'))

WebUI.click(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Next_1'))

CustomKeywords.'help.UploadFile.uploadFile'(findTestObject('Company/New Transaction/Transfer To Wallet/btn_Upload'), uploadFile)

WebUI.delay(1)

actual_row1 = WebUI.getText(findTestObject('Company/New Transaction/Transfer To Wallet/ValidateImport/validate_row1'))

WebUI.verifyEqual(actual_row1, expect_row1)

actual_row2 = WebUI.getText(findTestObject('Company/New Transaction/Transfer To Wallet/ValidateImport/validate_row2'))

WebUI.verifyEqual(actual_row2, expect_row2)

actual_row3 = WebUI.getText(findTestObject('Company/New Transaction/Transfer To Wallet/ValidateImport/validate_row3'))

WebUI.verifyEqual(actual_row3, expect_row3)

actual_row4 = WebUI.getText(findTestObject('Company/New Transaction/Transfer To Wallet/ValidateImport/validate_row4'))

WebUI.verifyEqual(actual_row4, expect_row4)

actual_row5 = WebUI.getText(findTestObject('Company/New Transaction/Transfer To Wallet/ValidateImport/validate_row5'))

WebUI.verifyEqual(actual_row5, expect_row5)

actual_row6 = WebUI.getText(findTestObject('Company/New Transaction/Transfer To Wallet/ValidateImport/validate_row6'))

WebUI.verifyEqual(actual_row6, expect_row6)

