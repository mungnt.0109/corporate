<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Update Profile</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>63ddae89-4d01-41cb-97df-0a51e2d4c84f</testSuiteGuid>
   <testCaseLink>
      <guid>163b3ca9-3891-4a67-86fb-8881a6e0d260</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Login/LG_001_001</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>07e81e85-958f-4e1b-9f89-15e7c8a561c5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>224976f8-248c-4a29-bdad-5dc39f1f93b7</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dfc87f8e-57fe-42db-948a-77f43f80488e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>69d5541c-f4a3-4dae-8261-99df859c1950</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a2123e9c-ae41-4fd0-8f50-3e9f2e71a44b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>ce542b04-6bdc-4a2b-a5d6-4d3fa11dafc6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Company/Update Profile/PRF_001_001</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>30004047-80b5-4047-93fa-3df3da6f18b0</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6e7be2a6-3c4a-48b9-8902-abc0261c900e</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>d6f369fa-2040-44b7-94a6-7b5ae1edc597</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>ae951946-62ad-4cbd-a36d-edfa5314b1bc</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>a11dfb9e-4892-4cff-a4d1-cd4d9d58bfaa</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>96bdf6f5-753c-4d08-b21e-8742908940e5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6aad0372-4f55-4df0-ab93-c247b0f6dcd7</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
