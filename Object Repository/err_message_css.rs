<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>err_message_css</name>
   <tag></tag>
   <elementGuidId>44d79d79-2576-4084-97cc-1dd28ff4cba5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.Toastify__toast-body > div</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>div.Toastify__toast-body > div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>css</name>
      <type>Main</type>
      <value>div.Toastify__toast-body > div</value>
   </webElementProperties>
</WebElementEntity>
